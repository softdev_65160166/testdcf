/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Test_service;

import com.dsi.dcoffee_project.model.Customer;
import com.dsi.dcoffee_project.service.CustomerService;

/**
 *
 * @author User
 */
public class TestCustomerService {
    public static void main(String[] args) {
        CustomerService cs = new CustomerService();
        for(Customer customer: cs.getCustomer()){
            System.out.println(customer);
        }
        System.out.println(cs.getByTel("0958256314"));
        Customer cus1 = new Customer("Plern", "0962576376",3);
        cs.addNew(cus1);
        for(Customer customer: cs.getCustomer()){
            System.out.println(customer);
        }
        Customer cus2 = new Customer("Adum", "0899998888", 10);
        cs.addNew(cus2);
        for(Customer customer: cs.getCustomer()){
            System.out.println(customer);
        }
        
        Customer delCus = cs.getByTel("0962576376");
        delCus.setTel("0962576367");
        cs.update(delCus);
        System.out.println("Update");
        for(Customer customer: cs.getCustomer()){
            System.out.println(customer);
        }
        
        System.out.println("Delete");
        cs.delete(delCus);
        for(Customer customer: cs.getCustomer()){
            System.out.println(customer);
        }
    }
}
 