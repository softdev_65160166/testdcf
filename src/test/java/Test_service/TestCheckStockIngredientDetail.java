/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Test_service;

import com.dsi.dcoffee_project.model.CheckStockIngredientDetail;
import com.dsi.dcoffee_project.service.CheckStockIngredientDetailService;

/**
 *
 * @author Surap
 */
public class TestCheckStockIngredientDetail {

    public static void main(String[] args) {
        CheckStockIngredientDetailService csid = new CheckStockIngredientDetailService();

        //CheckStockIngredientDetail csidAdd = new CheckStockIngredientDetail(2, 1, 15, 6, 4, 1600);
        //csid.addNew(csidAdd);
        
        //CheckStockIngredientDetail csidUpdate = csid.getById(2);
        //csidUpdate.setCheckLostMoney(400);
        //csid.update(csidUpdate);
        
        CheckStockIngredientDetail csidDel = csid.getById(3);
        csid.delete(csidDel);
        
        
        for (CheckStockIngredientDetail checkIngredient : csid.getCheckStockIngredientDetails()) {
            System.out.println(checkIngredient);
        }
        
        
    }
}
