/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.dsi.dcoffee_project.service;

import com.dsi.dcoffee_project.dao.ReceiptDao;
import com.dsi.dcoffee_project.dao.ReceiptDetailDao;
import com.dsi.dcoffee_project.model.Receipt;
import com.dsi.dcoffee_project.model.ReceiptDetail;
import java.util.List;

/**
 *
 * @author BoM
 */
public class ReceiptDetailService {
    public List<ReceiptDetail> getReceiptDetail(){
        ReceiptDetailDao receiptDetailDao = new ReceiptDetailDao();
        return receiptDetailDao.getAll("RD_ID asc");
    }

    public ReceiptDetail addNew(ReceiptDetail editedReceiptDetail) {
        ReceiptDetailDao receiptDetailDao = new ReceiptDetailDao();
        return receiptDetailDao.save(editedReceiptDetail);
    }

    public ReceiptDetail update(ReceiptDetail editedReceiptDetail) {
        ReceiptDetailDao receiptDetailDao = new ReceiptDetailDao();
        return receiptDetailDao.update(editedReceiptDetail);
    }

    public int delete(ReceiptDetail editedReceiptDetail) {
        ReceiptDetailDao receiptDetailDao = new ReceiptDetailDao();
        return receiptDetailDao.delete(editedReceiptDetail);
    }
}
