/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.dsi.dcoffee_project.service;

import com.dsi.dcoffee_project.dao.SalaryDao;
import com.dsi.dcoffee_project.dao.SalaryDao;
import com.dsi.dcoffee_project.dao.WorkTimeDao;
import com.dsi.dcoffee_project.model.Salary;
import com.dsi.dcoffee_project.model.Salary;
import com.dsi.dcoffee_project.model.WorkTime;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Surap
 */
public class SalaryService {
        public ArrayList<Salary> getAll() {
        SalaryDao salaryDao = new SalaryDao();
        return (ArrayList<Salary>) salaryDao.getAll();
    }
    public List<Salary> getUsersByOrder(String where, String order) {
        SalaryDao salaryDao = new SalaryDao();
        return salaryDao.getAll(where, order);
    }
    public Salary getById(int id) {
        SalaryDao SalaryDao = new SalaryDao();
        return SalaryDao.get(id);
    }
    
    public List<Salary> getSalarys(){
        SalaryDao salaryDao = new SalaryDao();
        return salaryDao.getAll("SLR_ID asc");
    }

    public Salary addNew(Salary editedSalary) {
        SalaryDao salaryDao = new SalaryDao();
        return salaryDao.save(editedSalary);
    }

    public Salary update(Salary editedSalary) {
        SalaryDao salaryDao = new SalaryDao();
        return salaryDao.update(editedSalary);
    }

    public int delete(Salary editedSalary) {
        SalaryDao salaryDao = new SalaryDao();
        return salaryDao.delete(editedSalary);
    }

}
