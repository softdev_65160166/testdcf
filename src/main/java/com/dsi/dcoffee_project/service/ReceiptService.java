/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.dsi.dcoffee_project.service;


import com.dsi.dcoffee_project.dao.ReceiptDao;
import com.dsi.dcoffee_project.dao.SalaryDao;
import com.dsi.dcoffee_project.model.Receipt;
import com.dsi.dcoffee_project.model.Salary;
import java.util.List;

/**
 *
 * @author BoM
 */
public class ReceiptService {
    
    public Receipt getById(int id) {
        ReceiptDao productDao = new ReceiptDao();
        return productDao.get(id);
    }
    
    public List<Receipt> getReceipt(){
        ReceiptDao receiptDao = new ReceiptDao();
        return receiptDao.getAll("R_ID asc");
    }

    public Receipt addNew(Receipt editedReceipt) {
        ReceiptDao receiptDao = new ReceiptDao();
        return receiptDao.save(editedReceipt);
    }

    public Receipt update(Receipt editedReceipt) {
        ReceiptDao receiptDao = new ReceiptDao();
        return receiptDao.update(editedReceipt);
    }

    public int delete(Receipt editedReceipt) {
        ReceiptDao receiptDao = new ReceiptDao();
        return receiptDao.delete(editedReceipt);
    }
    
}
