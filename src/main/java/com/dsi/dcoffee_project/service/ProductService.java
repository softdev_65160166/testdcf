/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.dsi.dcoffee_project.service;

import com.dsi.dcoffee_project.dao.ProductDao;
import com.dsi.dcoffee_project.dao.ProductDao;
import com.dsi.dcoffee_project.model.Ingredient;
import com.dsi.dcoffee_project.model.Product;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author BAM
 */
public class ProductService {
    private ProductDao productDao = new ProductDao();
    public ArrayList<Product> getProductOrderByName(){
       return (ArrayList<Product>)productDao.getAll("PD_NAME ASC");
    }
     public ArrayList<Product> getProductsCategoryDrink(){
        return (ArrayList<Product>) productDao.getAllCategory("Drink");
    }
    
    public ArrayList<Product> getProductsCategoryBakery(){
        return (ArrayList<Product>) productDao.getAllCategory("Bakery");
    }
    public Product getById(int id) {
        ProductDao productDao = new ProductDao();
        return productDao.getById(id);
    }

   
    public List<Product> getProducts(){
        ProductDao productDao = new ProductDao();
        return productDao.getAll("PD_ID asc");
    }

    
    public Product addNew(Product editedProduct) {
        ProductDao productDao = new ProductDao();
        return productDao.save(editedProduct);
    }

    public Product update(Product editedProduct) {
        ProductDao productDao = new ProductDao();
        return productDao.update(editedProduct);
    }

    public int delete(Product editedProduct) {
        ProductDao productDao = new ProductDao();
        return productDao.delete(editedProduct);
    }  
}
