/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.dsi.dcoffee_project.service;


import com.dsi.dcoffee_project.dao.IngredientReceiptDetailDao;
import com.dsi.dcoffee_project.model.IngredientReceiptDetail;
import java.util.List;

/**
 *
 * @author Surap
 */
public class IngredientReceiptDetailService {
    
    public IngredientReceiptDetail getById(int id) {
        IngredientReceiptDetailDao indrecdtDao = new IngredientReceiptDetailDao();
        return indrecdtDao.get(id);
    }
    
    public List<IngredientReceiptDetail> getIngredientReceiptDetail(){
        IngredientReceiptDetailDao indrecdtDao = new IngredientReceiptDetailDao();
        return indrecdtDao.getAll("INDRECDT_ID asc");
    }

    public IngredientReceiptDetail addNew(IngredientReceiptDetail editedIngredientReceiptDetail) {
        IngredientReceiptDetailDao indrecdtDao = new IngredientReceiptDetailDao();
        return indrecdtDao.save(editedIngredientReceiptDetail);
    }

    public IngredientReceiptDetail update(IngredientReceiptDetail editedIngredientReceiptDetail) {
        IngredientReceiptDetailDao indrecdtDao = new IngredientReceiptDetailDao();
        return indrecdtDao.update(editedIngredientReceiptDetail);
    }

    public int delete(IngredientReceiptDetail editedIngredientReceiptDetail) {
        IngredientReceiptDetailDao indrecdtDao = new IngredientReceiptDetailDao();
        return indrecdtDao.delete(editedIngredientReceiptDetail);
    }

}
