/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.dsi.dcoffee_project.service;

import com.dsi.dcoffee_project.dao.BranchDao;
import com.dsi.dcoffee_project.model.Branch;
import java.util.List;

/**
 *
 * @author Surap
 */
public class BranchService {
    
    public Branch getById(int id) {
        BranchDao branchDao = new BranchDao();
        return branchDao.get(id);
    }
    
    public Branch getByName(String name){
        BranchDao branchDao = new BranchDao();
        return branchDao.getByName(name);
    }
    
    public List<Branch> getBranchs(){
        BranchDao branchDao = new BranchDao();
        return branchDao.getAll("BR_ID asc");
    }

    public Branch addNew(Branch editedBranch) {
        BranchDao branchDao = new BranchDao();
        return branchDao.save(editedBranch);
    }

    public Branch update(Branch editedBranch) {
        BranchDao branchDao = new BranchDao();
        return branchDao.update(editedBranch);
    }

    public int delete(Branch editedBranch) {
        BranchDao branchDao = new BranchDao();
        return branchDao.delete(editedBranch);
    }

}
