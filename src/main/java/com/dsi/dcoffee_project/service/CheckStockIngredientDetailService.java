/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.dsi.dcoffee_project.service;

import com.dsi.dcoffee_project.dao.CheckStockIngredientDetailDao;
import com.dsi.dcoffee_project.model.CheckStockIngredientDetail;
import java.util.List;

/**
 *
 * @author Surap
 */
public class CheckStockIngredientDetailService {
    
    public CheckStockIngredientDetail getById(int id) {
        CheckStockIngredientDetailDao productDao = new CheckStockIngredientDetailDao();
        return productDao.get(id);
    }
    
    public List<CheckStockIngredientDetail> getCheckStockIngredientDetails(){
        CheckStockIngredientDetailDao checkStockIngredientDetailDao = new CheckStockIngredientDetailDao();
        return checkStockIngredientDetailDao.getAll("CHKD_ID asc");
    }
    
    public List<CheckStockIngredientDetail> getCSIDbyCheckID(int chkId){
        CheckStockIngredientDetailDao checkStockIngredientDetailDao = new CheckStockIngredientDetailDao();
        return checkStockIngredientDetailDao.getCSIDbyCheckID(chkId);
    }

    public CheckStockIngredientDetail addNew(CheckStockIngredientDetail editedCheckStockIngredientDetail) {
        CheckStockIngredientDetailDao checkStockIngredientDetailDao = new CheckStockIngredientDetailDao();
        return checkStockIngredientDetailDao.save(editedCheckStockIngredientDetail);
    }

    public CheckStockIngredientDetail update(CheckStockIngredientDetail editedCheckStockIngredientDetail) {
        CheckStockIngredientDetailDao checkStockIngredientDetailDao = new CheckStockIngredientDetailDao();
        return checkStockIngredientDetailDao.update(editedCheckStockIngredientDetail);
    }

    public int delete(CheckStockIngredientDetail editedCheckStockIngredientDetail) {
        CheckStockIngredientDetailDao checkStockIngredientDetailDao = new CheckStockIngredientDetailDao();
        return checkStockIngredientDetailDao.delete(editedCheckStockIngredientDetail);
    }

}
