/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.dsi.dcoffee_project.service;

import com.dsi.dcoffee_project.dao.EmployeeDao;
import com.dsi.dcoffee_project.model.Employee;
import java.util.List;

/**
 *
 * @author ASUS
 */
public class EmployeeService {

    public Employee login(String name, String password) {
        EmployeeDao empdao = new EmployeeDao();
        Employee emp = empdao.getByLogin(name);
        if (emp != null && emp.getEmp_password().equals(password)) {
            return emp;
        }
        return null;
    }
    
    public Employee getEmployeeBylogin(String name){
        EmployeeDao employeeDao = new EmployeeDao();
        return employeeDao.getByLogin(name);
    }
    public Employee getEmpByName(String name){
        EmployeeDao empDao = new EmployeeDao();
        return empDao.getByLogin(name);
    }
    public Employee getEmployeeById(int id){
        EmployeeDao employeeDao = new EmployeeDao();
        return employeeDao.get(id);
    }
    public List<Employee> getEmployee() {
        EmployeeDao EmployeeDao = new EmployeeDao();
        return EmployeeDao.getAll("EMP_ID asc");
    }

    public Employee addNew(Employee editEmployee) {
        EmployeeDao EmployeeDao = new EmployeeDao();
        return EmployeeDao.save(editEmployee);
    }

    public Employee update(Employee editEmployee) {
        EmployeeDao EmployeeDao = new EmployeeDao();
        return EmployeeDao.update(editEmployee);
    }

    public int delete(Employee editEmployee) {
        EmployeeDao EmployeeDao = new EmployeeDao();
        return EmployeeDao.delete(editEmployee);
    }
    public String getName(int id) {
        EmployeeDao employeeDao = new EmployeeDao();
        Employee employee = employeeDao.get(id);
        return employee.getEmp_name();
    }

}
