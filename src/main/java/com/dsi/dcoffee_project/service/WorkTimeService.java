/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.dsi.dcoffee_project.service;

import com.dsi.dcoffee_project.dao.WorkTimeDao;
import com.dsi.dcoffee_project.model.WorkTime;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 *
 * @author ASUS
 */
public class WorkTimeService {
    public ArrayList<WorkTime> getAll() {
        WorkTimeDao workTimeDao = new WorkTimeDao();
        return (ArrayList<WorkTime>) workTimeDao.getAll();
    }
    public WorkTime getById(int id) {
        WorkTimeDao workTimeDao = new WorkTimeDao();
        return workTimeDao.get(id);
    }
    public List<WorkTime> getUsersByOrder(String where, String order) {
        WorkTimeDao workTimeDao = new WorkTimeDao();
        return workTimeDao.getAll(where, order);
    }
    public WorkTime update(WorkTime editedWorkTime) {
        WorkTimeDao workTimeDao = new WorkTimeDao();
        return workTimeDao.update(editedWorkTime);
    }
 
    public WorkTime Save(WorkTime editedWorkTime) {
        WorkTimeDao workTimeDao = new WorkTimeDao();
        return workTimeDao.save(editedWorkTime);
    }
    public int delete(WorkTime editedWorkTime) {
        WorkTimeDao workTimeDao = new WorkTimeDao();
        return workTimeDao.delete(editedWorkTime);
    }
    
//    public List<WorkTime> getWorkTime(){
//        WorkTimeDao workTimeDao = new WorkTimeDao();
//        return workTimeDao.getAll("W_ID asc");
//    }
    
    public static String formatedDate(Date date) {
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm");
        String formatedDate = df.format(date);
        return formatedDate;
    }
    public static List<WorkTime> getWorkTime() {
        WorkTimeDao workTimeDao = new WorkTimeDao();
        return workTimeDao.getAll();
    }
}


