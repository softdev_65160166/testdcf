/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.dsi.dcoffee_project.dao;

import com.dsi.dcoffee_project.helper.DatabaseHelper;
import com.dsi.dcoffee_project.model.Receipt;
import com.dsi.dcoffee_project.model.ReceiptDetail;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author BoM
 */
public class ReceiptDetailDao implements Dao<ReceiptDetail>{
    @Override
    public List<ReceiptDetail> getAll() {
        ReceiptDetail receiptDetail = new ReceiptDetail();
        ArrayList<ReceiptDetail> list = new ArrayList();
        String sql = "SELECT * FROM RECEIPTDETAIL";
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                receiptDetail = ReceiptDetail.fromRS(rs);
                list.add(receiptDetail);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    @Override
    public List<ReceiptDetail> getAll(String where, String order) {
        ArrayList<ReceiptDetail> list = new ArrayList();
        String sql = "SELECT * FROM RECEIPTDETAIL where " + where + " ORDER BY " + order;
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                ReceiptDetail receiptDetail = ReceiptDetail.fromRS(rs);
                list.add(receiptDetail);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;     
    }
    
    @Override
    public ReceiptDetail get(int id) {
        ReceiptDetail receiptDetail = null;
        String sql = "SELECT * FROM RECEIPTDETAIL WHERE R_ID=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                receiptDetail = ReceiptDetail.fromRS(rs);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return receiptDetail;
    }
    
    public List<ReceiptDetail> getAll(String order) {
        ArrayList<ReceiptDetail> list = new ArrayList();
        String sql = "SELECT * FROM RECEIPTDETAIL ORDER BY " + order;
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                ReceiptDetail receiptDetail = ReceiptDetail.fromRS(rs);
                list.add(receiptDetail);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }
    
    @Override
    public ReceiptDetail save(ReceiptDetail obj) {
String sql = "INSERT INTO RECEIPT (R_ID, PD_ID, PT_ID, RD_QUANTITY, RD_PRICE, RD_DISCOUNT)"
                + "VALUES(?, ?, ?, ?, ?, ?)";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getReceiptId());
            stmt.setInt(2, obj.getProductId());
            stmt.setInt(3, obj.getPromotionId());
            stmt.setInt(4, obj.getReceiptDetailQuantity());
            stmt.setFloat(5, obj.getReceiptDetailPrice());
            stmt.setFloat(6, obj.getReceiptDetailDiscount());
//            System.out.println(stmt);
            stmt.executeUpdate();
            int id = DatabaseHelper.getInsertedId(stmt);
            obj.setReceiptDetailId(id);
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
        return obj;    
    }    

    @Override
    public ReceiptDetail update(ReceiptDetail obj) {
        String sql = "UPDATE RECEIPT"
                + "SET R_ID = ?, PD_ID = ?, PT_ID = ?, RD_QUANTITY = ?, RD_PRICE = ?, RD_DISCOUNT = ?"
                + " WHERE RD_ID = ?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getReceiptId());
            stmt.setInt(2, obj.getProductId());
            stmt.setInt(3, obj.getPromotionId());
            stmt.setInt(4, obj.getReceiptDetailQuantity());
            stmt.setFloat(5, obj.getReceiptDetailPrice());
            stmt.setFloat(6, obj.getReceiptDetailDiscount());
//            System.out.println(stmt);
            int ret = stmt.executeUpdate();
            System.out.println(ret);
            return obj;
            
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;    
        }
    }

    @Override
    public int delete(ReceiptDetail obj) {
        String sql = "DELETE FROM RECEIPTDETAIL WHERE RD_ID=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getReceiptDetailId());
            int ret = stmt.executeUpdate();
            return ret;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return -1;     
    }

    @Override
    public ReceiptDetail insert(ReceiptDetail obj) {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

  
    
}
