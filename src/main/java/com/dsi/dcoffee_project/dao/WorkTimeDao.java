/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.dsi.dcoffee_project.dao;

import com.dsi.dcoffee_project.helper.DatabaseHelper;
import com.dsi.dcoffee_project.model.WorkTime;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author ASUS
 */
public class WorkTimeDao implements Dao<WorkTime>{

    @Override
    public WorkTime get(int id) {
        WorkTime workTime = null;
        String sql = "SELECT * FROM WORKTIME WHERE W_ID =?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                workTime = WorkTime.fromRS(rs);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return workTime;
    }

    @Override
    public List<WorkTime> getAll() {
        ArrayList<WorkTime> list = new ArrayList();
        String sql = "SELECT * FROM WORKTIME ORDER BY EMP_ID";
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                WorkTime workTime = WorkTime.fromRS(rs);
                list.add(workTime);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    @Override
    public WorkTime save(WorkTime obj) {
        String sql = "INSERT INTO WORKTIME (EMP_ID, W_IN_TIME, W_OUT_TIME, W_TOTAL_TIME, W_STATUS, W_OT,W_MINIMUM)"
                + "VALUES(?, ?, ?, ?, ?,?,?)";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getEmployeeId());
            SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm");
            stmt.setString(2, df.format(obj.getCheckIn()));
            stmt.setString(3, df.format(obj.getCheckOut()));
            stmt.setInt(4, obj.getTotalWorked());
            stmt.setString(5, obj.getStatus());
            stmt.setInt(6, obj.getOverTime());
            stmt.setInt(7, obj.getMininum());
            stmt.executeUpdate();
            int id = DatabaseHelper.getInsertedId(stmt);
            obj.setId(id);
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
        return obj;
    }

    @Override
    public WorkTime update(WorkTime obj) {
        String sql = "UPDATE WORKTIME"
                + " SET EMP_ID = ?, W_IN_TIME = ?, W_OUT_TIME = ?, W_TOTAL_TIME = ?,W_STATUS = ?, W_OT = ?,W_MINIMUM = ?"
                + " WHERE W_ID = ?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getEmployeeId());
            SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm");
            stmt.setString(2, df.format(obj.getCheckIn()));
            stmt.setString(3, df.format(obj.getCheckOut()));
            stmt.setInt(4, obj.getTotalWorked());
            stmt.setString(5, obj.getStatus());
            stmt.setInt(6, obj.getOverTime());
            stmt.setInt(7, obj.getMininum());
            stmt.setInt(8, obj.getId());
            
            stmt.executeUpdate();
            int ret = stmt.executeUpdate();
            return obj;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
    }
    }

    @Override
    public int delete(WorkTime obj) {
        String sql = "DELETE FROM WORKTIME WHERE W_ID =?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getId());
            int ret = stmt.executeUpdate();
            return ret;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return -1;
    }

    @Override
    public List<WorkTime> getAll(String where, String order) {
        ArrayList<WorkTime> list = new ArrayList();
        String sql = "SELECT * FROM WORKTIME WHERE " + where + " ORDER BY " + order;
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                WorkTime workTime = WorkTime.fromRS(rs);
                list.add(workTime);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
        }

    @Override
    public WorkTime insert(WorkTime obj) {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }
    
    public List<WorkTime> getAll(String order) {
        ArrayList<WorkTime> list = new ArrayList();
        String sql = "SELECT * FROM WORKTIME ORDER BY " + order;
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                WorkTime workTime = WorkTime.fromRS(rs);
                list.add(workTime);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }
    
    public WorkTime getById(int id) {
        WorkTime workTime = null;
        String sql = "SELECT * FROM WORKTIME WHERE W_ID=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                workTime = WorkTime.fromRS(rs);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return workTime;
    }
}
