package com.dsi.dcoffee_project.dao;

import com.dsi.dcoffee_project.helper.DatabaseHelper;
import com.dsi.dcoffee_project.model.Vendor;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */

/**
 *
 * @author BAM
 */
public class VendorDao implements Dao<Vendor> {
    
    @Override
    public Vendor get(int id) {
        Vendor vendor = null;
        String sql = "SELECT * FROM VENDOR WHERE VD_ID=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                vendor = Vendor.fromRS(rs);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return vendor;
    }
    
    public Vendor getByName(String name) {
        Vendor vendor = null;
        String sql = "SELECT * FROM VENDOR WHERE VD_NAME=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, name);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                vendor = Vendor.fromRS(rs);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return vendor;
    }

    @Override
    public List<Vendor> getAll() {
        ArrayList<Vendor> list = new ArrayList();
        String sql = "SELECT * FROM VENDOR";
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                Vendor vendor = Vendor.fromRS(rs);
                list.add(vendor);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }
    
    @Override
    public List<Vendor> getAll(String where, String order) {
        ArrayList<Vendor> list = new ArrayList();
        String sql = "SELECT * FROM VENDOR where " + where + " ORDER BY " + order;
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                Vendor vendor = Vendor.fromRS(rs);
                list.add(vendor);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }
    

    public List<Vendor> getAll(String order) {
        ArrayList<Vendor> list = new ArrayList();
        String sql = "SELECT * FROM VENDOR ORDER BY " + order;
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                Vendor vendor = Vendor.fromRS(rs);
                list.add(vendor);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    @Override
    public Vendor save(Vendor obj) {

        String sql = "INSERT INTO VENDOR (VD_NAME, VD_ADDRESS, VD_TEL, VD_EMAIL)"
                + "VALUES(?, ?, ?, ?)";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, obj.getName());
            stmt.setString(2, obj.getAddress());
            stmt.setString(3, obj.getTel());
            stmt.setString(4, obj.getEmail());
            
//            System.out.println(stmt);
            stmt.executeUpdate();
            int id = DatabaseHelper.getInsertedId(stmt);
            obj.setId(id);
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
        return obj;
    }

    @Override
    public Vendor update(Vendor obj) {
        String sql = "UPDATE VENDOR"
                + " SET VD_NAME = ?, VD_ADDRESS = ?, VD_TEL = ?, VD_EMAIL = ?"
                + " WHERE VD_ID = ?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, obj.getName());
            stmt.setString(2, obj.getAddress());
            stmt.setString(3, obj.getTel());
            stmt.setString(4, obj.getEmail());
            stmt.setInt(5, obj.getId());
            
//            System.out.println(stmt);
            int ret = stmt.executeUpdate();
            System.out.println(ret);
            return obj;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
    }

    @Override
    public int delete(Vendor obj) {
        String sql = "DELETE FROM VENDOR WHERE VD_ID=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getId());
            int ret = stmt.executeUpdate();
            return ret;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return -1;        
    }

    @Override
    public Vendor insert(Vendor obj) {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    
}
