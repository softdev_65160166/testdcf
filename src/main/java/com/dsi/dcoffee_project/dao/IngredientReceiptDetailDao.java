/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.dsi.dcoffee_project.dao;

import com.dsi.dcoffee_project.helper.DatabaseHelper;
import com.dsi.dcoffee_project.model.IngredientReceiptDetail;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author sirapob
 */
public class IngredientReceiptDetailDao implements Dao<IngredientReceiptDetail> {

    @Override
    public IngredientReceiptDetail get(int id) {
        IngredientReceiptDetail indrecdt = null;
        String sql = "SELECT * FROM INGREDIENTRECEIPTDETAIL WHERE INDRECDT_ID=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                indrecdt = IngredientReceiptDetail.fromRS(rs);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return indrecdt;
    } // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody

    public IngredientReceiptDetail getByLogin(String name) {
        IngredientReceiptDetail indrecdt = null;
        String sql = "SELECT * FROM EMPLOYEE WHERE EMP_NAME=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, name);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                indrecdt = IngredientReceiptDetail.fromRS(rs);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return indrecdt;
    }

    @Override
    public List<IngredientReceiptDetail> getAll() {
        ArrayList<IngredientReceiptDetail> list = new ArrayList();
        String sql = "SELECT * FROM INGREDIENTRECEIPTDETAIL";
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                IngredientReceiptDetail indrecdt = IngredientReceiptDetail.fromRS(rs);
                list.add(indrecdt);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list; // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    @Override
    public List<IngredientReceiptDetail> getAll(String where, String order) {
        ArrayList<IngredientReceiptDetail> list = new ArrayList();
        String sql = "SELECT * FROM INGREDIENTRECEIPTDETAIL where " + where + " ORDER BY " + order;
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                IngredientReceiptDetail indrecdt = IngredientReceiptDetail.fromRS(rs);
                list.add(indrecdt);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    public List<IngredientReceiptDetail> getAll(String order) {
        ArrayList<IngredientReceiptDetail> list = new ArrayList();
        String sql = "SELECT * FROM INGREDIENTRECEIPTDETAIL ORDER BY " + order;
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                IngredientReceiptDetail indrecdt = IngredientReceiptDetail.fromRS(rs);
                list.add(indrecdt);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    @Override
    public IngredientReceiptDetail save(IngredientReceiptDetail obj) {

        String sql = "INSERT INTO INGREDIENTRECEIPTDETAIL ( IND_ID, IGRT_ID, INDRECDT_MFD, INDRECDT_EXD ) "
                + "VALUES (?,?,?,?);";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1,obj.getInd_id());
            stmt.setInt(2, obj.getIgrt_id());
            stmt.setString(3,obj.getIndrecdt_mfd());
            stmt.setString(4,obj.getIndrecdt_exd());
            stmt.executeUpdate();
            int id = DatabaseHelper.getInsertedId(stmt);
            obj.setIndrecdt_id(id);

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
        return obj;
        // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    @Override
    public IngredientReceiptDetail update(IngredientReceiptDetail obj) {
        String sql = "UPDATE INGREDIENTRECEIPTDETAIL SET IND_ID = ?, IGRT_ID = ?, INDRECDT_MFD = ?, INDRECDT_EXD = ? WHERE INDRECDT_ID = ?;";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1,obj.getInd_id());
            stmt.setInt(2, obj.getIgrt_id());
            stmt.setString(3,obj.getIndrecdt_mfd());
            stmt.setString(4,obj.getIndrecdt_exd());
            stmt.setInt(5,obj.getIndrecdt_id());
            int ret = stmt.executeUpdate();
            System.out.println(ret);
            return obj;

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
    }

    @Override
    public int delete(IngredientReceiptDetail obj) {
        String sql = "DELETE FROM INGREDIENTRECEIPTDETAIL WHERE INDRECDT_ID=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getIndrecdt_id());
            int ret = stmt.executeUpdate();
            return ret;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return -1; // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    @Override
    public IngredientReceiptDetail insert(IngredientReceiptDetail obj) {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

}
