/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.dsi.dcoffee_project.dao;

import com.dsi.dcoffee_project.helper.DatabaseHelper;
import com.dsi.dcoffee_project.model.Employee;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author sirapob
 */
public class EmployeeDao implements Dao<Employee> {

    @Override
    public Employee get(int id) {
        Employee emp = null;
        String sql = "SELECT * FROM EMPLOYEE WHERE EMP_ID=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                emp = Employee.fromRS(rs);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return emp;
    } // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody

    public Employee getByLogin(String name) {
        Employee emp = null;
        String sql = "SELECT * FROM EMPLOYEE WHERE EMP_NAME=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, name);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                emp = Employee.fromRS(rs);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return emp;
    }
    
    @Override
    public List<Employee> getAll() {
        ArrayList<Employee> list = new ArrayList();
        String sql = "SELECT * FROM EMPLOYEE";
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                Employee emp = Employee.fromRS(rs);
                list.add(emp);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list; // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    @Override
    public List<Employee> getAll(String where, String order) {
        ArrayList<Employee> list = new ArrayList();
        String sql = "SELECT * FROM EMPLOYEE where " + where + " ORDER BY " + order;
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                Employee emp = Employee.fromRS(rs);
                list.add(emp);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    public List<Employee> getAll(String order) {
        ArrayList<Employee> list = new ArrayList();
        String sql = "SELECT * FROM EMPLOYEE ORDER BY " + order;
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                Employee emp = Employee.fromRS(rs);
                list.add(emp);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    @Override
    public Employee save(Employee obj) {

        String sql = "INSERT INTO EMPLOYEE (BR_ID,EMP_NAME,EMP_EMAIL,EMP_PHONE_NUMBER,EMP_ADDRESS,EMP_POSITION,EMP_WAGE,EMP_TYPE,EMP_PASSWORD,EMP_SALARY)"
                + " VALUES (?,?,?,?,?,?,?,?,?,?);";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getBr_id());
            stmt.setString(2, obj.getEmp_name());
            stmt.setString(3, obj.getEmp_email());
            stmt.setString(4, obj.getEmp_phone_number());
            stmt.setString(5, obj.getEmp_address());
            stmt.setString(6, obj.getEmp_position());
            stmt.setFloat(7, obj.getEmp_wage());
            stmt.setString(8, obj.getEmp_type());
            stmt.setString(9, obj.getEmp_password());
            stmt.setFloat(10, obj.getEmp_salary());
            stmt.executeUpdate();
            int id = DatabaseHelper.getInsertedId(stmt);
            obj.setEmp_id(id);

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
        return obj;
        // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    @Override
    public Employee update(Employee obj) {
        String sql = "UPDATE EMPLOYEE "
                + "SET BR_ID = ?,EMP_NAME = ?, EMP_EMAIL = ?, EMP_PHONE_NUMBER = ?, EMP_ADDRESS = ?, EMP_POSITION = ?, EMP_WAGE = ?, EMP_TYPE = ?, EMP_PASSWORD = ?, EMP_SALARY = ?"
                + "WHERE EMP_ID = ?;";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getBr_id());
            stmt.setString(2, obj.getEmp_name());
            stmt.setString(3, obj.getEmp_email());
            stmt.setString(4, obj.getEmp_phone_number());
            stmt.setString(5, obj.getEmp_address());
            stmt.setString(6, obj.getEmp_position());
            stmt.setFloat(7, obj.getEmp_wage());
            stmt.setString(8, obj.getEmp_type());
            stmt.setString(9, obj.getEmp_password());
            stmt.setFloat(10, obj.getEmp_salary());
            stmt.setInt(11, obj.getEmp_id());
            int ret = stmt.executeUpdate();
            System.out.println(ret);
            return obj;

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
    }

    @Override
    public int delete(Employee obj) {
        String sql = "DELETE FROM EMPLOYEE WHERE EMP_ID=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getEmp_id());
            int ret = stmt.executeUpdate();
            return ret;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return -1; 
    }

    @Override
    public Employee insert(Employee obj) {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

}
