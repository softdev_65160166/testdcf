/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */

package com.dsi.dcoffee_project.model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;


/**
 *
 * @author Tanakorn
 */
public class Promotion {
    private int id;
    private String name;
    private Date startDate;
    private Date endDate;
    private boolean status;
    private int discount;

    public Promotion(int id, String name, Date startDate, Date endDate, boolean status, int discount) {
        this.id = id;
        this.name = name;
        this.startDate = startDate;
        this.endDate = endDate;
        this.status = status;
        this.discount = discount;
    }
    
    public Promotion(String name, Date startDate, Date endDate, boolean status, int discount) {
        this.id = -1;
        this.name = name;
        this.startDate = startDate;
        this.endDate = endDate;
        this.status = status;
        this.discount = discount;
    }
    
    public Promotion() {
        this.id = -1;
        this.name = "";
        this.startDate = null;
        this.endDate = null;
        this.status = false;
        this.discount = 0;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public int getDiscount() {
        return discount;
    }

    public void setDiscount(int discount) {
        this.discount = discount;
    }

    @Override
    public String toString() {
        return "Promotion{" + "id=" + id + ", name=" + name + ", startDate=" + startDate + ", endDate=" + endDate + ", status=" + status + ", discount=" + discount + '}';
    }
    
    

    
  
    public static Promotion fromRS(ResultSet rs) {
        Promotion product = new Promotion();
        try {
            product.setId(rs.getInt("PT_ID"));
            product.setName(rs.getString("PT_NAME"));
            product.setStartDate(rs.getDate("PT_START_DATE"));
            product.setEndDate(rs.getDate("PT_END_DATE"));
            product.setStatus(rs.getBoolean("PT_STATUS"));
            product.setDiscount(rs.getInt("PT_DISCOUNT"));
            
        } catch (SQLException ex) {
            Logger.getLogger(Promotion.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return product;
    }
}
