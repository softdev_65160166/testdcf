/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.dsi.dcoffee_project.model;


import java.util.Date;
import java.text.SimpleDateFormat;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.logging.Level;
import java.util.logging.Logger;
/**
 *
 * @author ASUS
 */
public class WorkTime {
    private int id;
    private int EmployeeId;
    private Date checkIn;
    private Date checkOut;
    private int totalWorked;
    private String status;
    private int overTime;
    private int mininum;

    public WorkTime(int id, int EmployeeId,Date checkIn, Date checkOut, int totalWorked, String status, int overTime,int mininum) {
        this.id = id;
        this.EmployeeId = EmployeeId;
        this.checkIn = checkIn;
        this.checkOut = checkOut;
        this.totalWorked = totalWorked;
        this.status = status;
        this.overTime = overTime;
        this.mininum = mininum;
    }
    
    public WorkTime(int EmployeeId,Date checkIn, Date checkOut, int totalWorked, String status, int overTime,int mininum) {
        this.id = -1;
        this.EmployeeId = EmployeeId;
        this.checkIn = checkIn;
        this.checkOut = checkOut;
        this.totalWorked = totalWorked;
        this.status = status;
        this.overTime = overTime;
        this.mininum = mininum;
    }
    
    public WorkTime() {
        this.id = -1;
    }


    public int getMininum() {
        return mininum;
    }

    public void setMininum(int mininum) {
        this.mininum = mininum;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getOverTime() {
        return overTime;
    }

    public void setOverTime(int overTime) {
        this.overTime = overTime;
    }

    public int getEmployeeId() {
        return EmployeeId;
    }

    public void setEmployeeId(int EmployeeId) {
        this.EmployeeId = EmployeeId;
    }

    public Date getCheckIn() {
        return checkIn;
    }

    public void setCheckIn(Date checkIn) {
        this.checkIn = checkIn;
    }

    public Date getCheckOut() {
        return checkOut;
    }

    public void setCheckOut(Date checkOut) {
        this.checkOut = checkOut;
    }

    public int getTotalWorked() {
        return totalWorked;
    }

    public void setTotalWorked(int totalWorked) {
        this.totalWorked = totalWorked;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "WorkTime{" + "id=" + id + ", EmployeeId=" + EmployeeId + ", checkIn=" + checkIn + ", checkOut=" + checkOut + ", totalWorked=" + totalWorked + ", status=" + status + ", overTime=" + overTime + ", mininum=" + mininum + '}';
    }


    
    public static WorkTime fromRS(ResultSet rs) {
        WorkTime workTime = new WorkTime();
        try {
            workTime.setId(rs.getInt("W_ID"));
            workTime.setEmployeeId(rs.getInt("EMP_ID"));
            SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm");
            String in = rs.getString("W_IN_TIME");
            String out = rs.getString("W_OUT_TIME");
            try {
                workTime.setCheckIn(df.parse(in));
                workTime.setCheckOut(df.parse(out));
            } catch (ParseException ex) {
                Logger.getLogger(WorkTime.class.getName()).log(Level.SEVERE, null, ex);
            }
            workTime.setTotalWorked(rs.getInt("W_TOTAL_TIME"));
            workTime.setStatus(rs.getString("W_STATUS"));
            workTime.setOverTime(rs.getInt("W_OT"));
            workTime.setMininum(rs.getInt("W_MINIMUM"));
            
        } catch (SQLException ex) {
            Logger.getLogger(WorkTime.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return workTime;
    }
    
}
