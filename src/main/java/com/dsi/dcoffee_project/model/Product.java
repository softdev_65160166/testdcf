/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */

package com.dsi.dcoffee_project.model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;


/**
 *
 * @author Surap
 */
public class Product {
    private int id;
    private String name;
    private float price;
    private String category;
    private String subCategory;
    private String size;
    private int quantity;
    private String sweetLevel;

    public Product(int id, String name, float price, String category, String subCategory, String size, int quantity, String sweetLevel) {
        this.id = id;
        this.name = name;
        this.price = price;
        this.category = category;
        this.subCategory = subCategory;
        this.size = size;
        this.quantity = quantity;
        this.sweetLevel = sweetLevel;
    }

    public Product(String name, float price, String category, String subCategory, String size, int quantity, String sweetLevel) {
        this.id = -1;
        this.name = name;
        this.price = price;
        this.category = category;
        this.subCategory = subCategory;
        this.size = size;
        this.quantity = quantity;
        this.sweetLevel = sweetLevel;
    }

    public Product() {
        this.id = -1;
        this.name = "";
        this.price = 0;
        this.category = "";
        this.subCategory = "";
        this.size = "";
        this.quantity = 0;
        this.sweetLevel = "";
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getSubCategory() {
        return subCategory;
    }

    public void setSubCategory(String subCategory) {
        this.subCategory = subCategory;
    }

    public String getSize() {
        return size;
    }

    public void setSize(String size) {
        this.size = size;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public String getSweetLevel() {
        return sweetLevel;
    }

    public void setSweetLevel(String sweetLevel) {
        this.sweetLevel = sweetLevel;
    }

    @Override
    public String toString() {
        return "Product{" + "id=" + id + ", name=" + name + ", price=" + price + ", category=" + category + ", subCategory=" + subCategory + ", size=" + size + ", quantity=" + quantity + ", sweetLevel=" + sweetLevel + '}';
    }
    
    public static Product fromRS(ResultSet rs) {
        Product product = new Product();
        try {
            product.setId(rs.getInt("PD_ID"));
            product.setName(rs.getString("PD_NAME"));
            product.setPrice(rs.getFloat("PD_PRICE"));
            product.setCategory(rs.getString("PD_CATEGORY"));
            product.setSubCategory(rs.getString("PD_SUBCATEGORY"));
            product.setSize(rs.getString("PD_SIZE"));
            product.setQuantity(rs.getInt("PD_QUANTITY"));
            product.setSweetLevel(rs.getString("PD_SWEET_LEVEL"));
        } catch (SQLException ex) {
            Logger.getLogger(Product.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return product;
    }
}
