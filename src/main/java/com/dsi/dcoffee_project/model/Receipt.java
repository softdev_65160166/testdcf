/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.dsi.dcoffee_project.model;


import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author BoM
 */
public class Receipt {
        private int id;
        private int BranchId;
        private int EmployeeId;
        private int CustomerId;
        private String Date;
        private int totalQty;
        private float total;
        private float Discount;
        private float MoneyReceived;
        private float Change;
        private String Payment;
        private ArrayList<ReceiptDetail> receiptDetails = new ArrayList();

    public Receipt(int id, int BranchId, int EmployeeId, int CustomerId, String Date,int totalQty, float total, float Discount, float MoneyReceived, float Change, String Payment) {
        this.id = id;
        this.BranchId = BranchId;
        this.EmployeeId = EmployeeId;
        this.CustomerId = CustomerId;
        this.Date = Date;
        this.totalQty = totalQty;
        this.total = total;
        this.Discount = Discount;
        this.MoneyReceived = MoneyReceived;
        this.Change = Change;
        this.Payment = Payment;
    }

    public Receipt( int BranchId, int EmployeeId, int CustomerId, String Date,int totalQty, float total, float Discount, float MoneyReceived, float Change, String Payment) {
        this.id = -1;
        this.BranchId = BranchId;
        this.EmployeeId = EmployeeId;
        this.CustomerId = CustomerId;
        this.Date = Date;
        this.totalQty = totalQty;
        this.total = total;
        this.Discount = Discount;
        this.MoneyReceived = MoneyReceived;
        this.Change = Change;
        this.Payment = Payment;
    }
    
    public Receipt() {
        this.id = -1;
        this.BranchId = 0;
        this.EmployeeId = 0;
        this.CustomerId = 0;
        this.Date = "";
        this.totalQty = 0;
        this.total = 0;
        this.Discount = 0;
        this.MoneyReceived = 0;
        this.Change = 0;
        this.Payment = "";
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getBranchId() {
        return BranchId;
    }

    public void setBranchId(int BranchId) {
        this.BranchId = BranchId;
    }

    public int getEmployeeId() {
        return EmployeeId;
    }

    public void setEmployeeId(int EmployeeId) {
        this.EmployeeId = EmployeeId;
    }

    public int getCustomerId() {
        return CustomerId;
    }

    public void setCustomerId(int CustomerId) {
        this.CustomerId = CustomerId;
    }

    public String getDate() {
        return Date;
    }

    public void setString(String Date) {
        this.Date = Date;
    }

    public int getTotalQty() {
        return totalQty;
    }

    public void setTotalQty(int totalQty) {
        this.totalQty = totalQty;
    }

    public float getTotal() {
        return total;
    }

    public void setTotal(float total) {
        this.total = total;
    }


    public float getDiscount() {
        return Discount;
    }

    public void setDiscount(float Discount) {
        this.Discount = Discount;
    }

    public float getMoneyReceived() {
        return MoneyReceived;
    }

    public void setMoneyReceived(float MoneyReceived) {
        this.MoneyReceived = MoneyReceived;
    }

    public float getChange() {
        return Change;
    }

    public void setChange(float Change) {
        this.Change = Change;
    }

    public String getPayment() {
        return Payment;
    }

    public void setPayment(String Payment) {
        this.Payment = Payment;
    }
    
    public ArrayList<ReceiptDetail> getReceiptDetails() {
        return receiptDetails;
    }

    public void setReceiptDetails(ArrayList<ReceiptDetail> receiptDetails) {
        this.receiptDetails = receiptDetails;
    }
    
    public void addReceiptDetail(ReceiptDetail receiptDetail){
        receiptDetails.add(receiptDetail);
        
    }
    
    
    public void delReceoptDetail(ReceiptDetail receiptDetail) {
        receiptDetails.remove(receiptDetail);
        calculateTotal();
    }

    private void calculateTotal() {
        int totalQty = 0;
        float total = 0.0f;
        for (ReceiptDetail rd : receiptDetails) {
            total += rd.getReceiptDetailQuantity() * rd.getReceiptDetailPrice();
            totalQty += rd.getReceiptDetailQuantity();
        }
        this.totalQty = totalQty;
        this.total = total;
    }
    
    @Override
    public String toString() {
        return "Receipt{" + "id=" + id + ", BranchId=" + BranchId + ", EmployeeId=" + EmployeeId + ", CustomerId=" + CustomerId + ", Date=" + Date + ", totalQty=" + totalQty + ", total=" + total + ", Discount=" + Discount + ", MoneyReceived=" + MoneyReceived + ", Change=" + Change + ", Payment=" + Payment + ", receiptDetails=" + receiptDetails + '}';
    }
    public static Receipt fromRS(ResultSet rs) {
            Receipt receipt = new Receipt();
            try {
                receipt.setId(rs.getInt("R_ID"));
                receipt.setBranchId(rs.getInt("BR_ID"));
                receipt.setEmployeeId(rs.getInt("EMP_ID"));
                receipt.setCustomerId(rs.getInt("CUS_ID"));
                receipt.setString(rs.getString("R_DATE"));
                receipt.setTotal(rs.getInt("R_TOTAL"));
                receipt.setDiscount(rs.getFloat("R_DISCOUNT"));
                receipt.setMoneyReceived(rs.getFloat("R_MONEYRECEIVED"));
                receipt.setChange(rs.getFloat("R_CHANGE"));
                receipt.setPayment(rs.getString("R_PAYMENT"));
            } catch (SQLException ex) {
                Logger.getLogger(Receipt.class.getName()).log(Level.SEVERE, null, ex);
                return null;
            }
            return receipt;
        }

    

   
    }


