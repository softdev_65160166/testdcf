/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.dsi.dcoffee_project.model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Surap
 */
public class CheckStockIngredientDetail {
    private int id;
    private int checkStockId;
    private int ingredeintId;
    private int checkLast;
    private int checkRemain;


    public CheckStockIngredientDetail(int id, int checkId, int ingredeintId, int checkLast, int checkRemain) {
        this.id = id;
        this.checkStockId = checkId;
        this.ingredeintId = ingredeintId;
        this.checkLast = checkLast;
        this.checkRemain = checkRemain;

    }
    
    public CheckStockIngredientDetail( int checkId, int ingredeintId, int checkLast, int checkRemain) {
        this.id = -1;
        this.checkStockId = checkId;
        this.ingredeintId = ingredeintId;
        this.checkLast = checkLast;
        this.checkRemain = checkRemain;

    }
    
    public CheckStockIngredientDetail() {
        this.id = -1;
        this.checkStockId = 0;
        this.ingredeintId = 0;
        this.checkLast = 0;
        this.checkRemain = 0;

    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getCheckStockId() {
        return checkStockId;
    }

    public void setCheckStockId(int checkStockId) {
        this.checkStockId = checkStockId;
    }

    public int getIngredeintId() {
        return ingredeintId;
    }

    public void setIngredeintId(int ingredeintId) {
        this.ingredeintId = ingredeintId;
    }

    public int getCheckLast() {
        return checkLast;
    }

    public void setCheckLast(int checkLast) {
        this.checkLast = checkLast;
    }

    public int getCheckRemain() {
        return checkRemain;
    }

    public void setCheckRemain(int checkRemain) {
        this.checkRemain = checkRemain;
    }

    @Override
    public String toString() {
        return "CheckStockIngredientDetail{" + "id=" + id + ", checkStockId=" + checkStockId + ", ingredeintId=" + ingredeintId + ", checkLast=" + checkLast + ", checkRemain=" + checkRemain + '}';
    }




    
    
    public static CheckStockIngredientDetail fromRS(ResultSet rs) {
        CheckStockIngredientDetail csid = new CheckStockIngredientDetail();
        try {
            csid.setId(rs.getInt("CHKD_ID"));
            csid.setCheckStockId(rs.getInt("CHK_ID"));
            csid.setIngredeintId(rs.getInt("IND_ID"));
            csid.setCheckLast(rs.getInt("CHKD_LAST"));
            csid.setCheckRemain(rs.getInt("CHKD_REMAIN"));

        } catch (SQLException ex) {
            Logger.getLogger(CheckStockIngredientDetail.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return csid;
    
    }}
