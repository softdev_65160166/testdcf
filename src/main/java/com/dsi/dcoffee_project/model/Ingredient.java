/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.dsi.dcoffee_project.model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author BAM
 */
public class Ingredient {
    private int id;
    private String name;
    private float price;
    private int qty;
    private int min;
    private String unit;

    public Ingredient(int id, String name, float price, int qty, int min, String unit) {
        this.id = id;
        this.name = name;
        this.price = price;
        this.qty = qty;
        this.min = min;
        this.unit = unit;
    }

    public Ingredient(String name, float price, int qty, int min, String unit) {
        this.id = -1;
        this.name = name;
        this.price = price;
        this.qty = qty;
        this.min = min;
        this.unit = unit;
    }

    public Ingredient() {
        this.id = -1;
        this.name = "";
        this.price = 0;
        this.qty = 0;
        this.min = 0;
        this.unit = "";
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }

    public int getQty() {
        return qty;
    }

    public void setQty(int qty) {
        this.qty = qty;
    }

    public int getMin() {
        return min;
    }

    public void setMin(int min) {
        this.min = min;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    @Override
    public String toString() {
        return "Ingredient{" + "id=" + id + ", name=" + name + ", price=" + price + ", qty=" + qty + ", min=" + min + ", unit=" + unit + '}';
    }
    
    
    public static Ingredient fromRS(ResultSet rs) {
        Ingredient ingredient = new Ingredient();
        try {
            ingredient.setId(rs.getInt("IND_ID"));
            ingredient.setName(rs.getString("IND_NAME"));
            ingredient.setPrice(rs.getFloat("IND_PRICE"));
            ingredient.setQty(rs.getInt("IND_QUANTITY"));
            ingredient.setMin(rs.getInt("IND_MINIMUM"));
            ingredient.setUnit(rs.getString("IND_UNIT"));
        } catch (SQLException ex) {
            Logger.getLogger(Ingredient.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return ingredient;
    }
    
}
