/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.dsi.dcoffee_project.model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author BoM
 */
public class ReceiptDetail {
    private int ReceiptDetailId;
    private int ReceiptId;
    private int ProductId;
    private Product product;
    private Receipt receipt;
    private int PromotionId;
    private int ReceiptDetailQuantity;
    private float ReceiptDetailPrice;
    private float ReceiptDetailDiscount;
    private float TotalPrice;

    public ReceiptDetail(int ReceiptDetailId, int ReceiptId, int ProductId, Product product,Receipt receipt,int PromotionId, int ReceiptDetailQuantity, float ReceiptDetailPrice, float ReceiptDetailDiscount, float TotalPrice) {
        this.ReceiptDetailId = ReceiptDetailId;
        this.ReceiptId = ReceiptId;
        this.ProductId = ProductId;
        this.product = product;
        this.receipt = receipt;
        this.PromotionId = PromotionId;
        this.ReceiptDetailQuantity = ReceiptDetailQuantity;
        this.ReceiptDetailPrice = ReceiptDetailPrice;
        this.ReceiptDetailDiscount = ReceiptDetailDiscount;
        this.TotalPrice = TotalPrice;
    }

    public ReceiptDetail( int ReceiptId, int ProductId, Product product,Receipt receipt,int PromotionId, int ReceiptDetailQuantity, float ReceiptDetailPrice, float ReceiptDetailDiscount, float TotalPrice) {
        this.ReceiptDetailId = -1;
        this.ReceiptId = ReceiptId;
        this.ProductId = ProductId;
        this.product = product;
        this.receipt = receipt;
        this.PromotionId = PromotionId;
        this.ReceiptDetailQuantity = ReceiptDetailQuantity;
        this.ReceiptDetailPrice = ReceiptDetailPrice;
        this.ReceiptDetailDiscount = ReceiptDetailDiscount;
        this.TotalPrice = TotalPrice;
    }
    
    public ReceiptDetail() {
        this.ReceiptDetailId = -1;
        this.ReceiptId = 0;
        this.ProductId = 0;
        this.product = null;
        this.receipt = null;
        this.PromotionId = 0;
        this.ReceiptDetailQuantity = 0;
        this.ReceiptDetailPrice = 0;
        this.ReceiptDetailDiscount = 0;
        this.TotalPrice = 0;
        
    }

    public int getReceiptDetailId() {
        return ReceiptDetailId;
    }

    public void setReceiptDetailId(int ReceiptDetailId) {
        this.ReceiptDetailId = ReceiptDetailId;
    }

    public int getReceiptId() {
        return ReceiptId;
    }

    public void setReceiptId(int ReceiptId) {
        this.ReceiptId = ReceiptId;
    }

    public int getProductId() {
        return ProductId;
    }

    public void setProductId(int ProductId) {
        this.ProductId = ProductId;
    }

    public int getPromotionId() {
        return PromotionId;
    }

    public void setPromotionId(int PromotionId) {
        this.PromotionId = PromotionId;
    }

    public int getReceiptDetailQuantity() {
        return ReceiptDetailQuantity;
    }

    public void setReceiptDetailQuantity(int ReceiptDetailQuantity) {
        this.ReceiptDetailQuantity = ReceiptDetailQuantity;
        TotalPrice = ReceiptDetailQuantity * ReceiptDetailPrice;
    }

    public float getReceiptDetailPrice() {
        return ReceiptDetailPrice;
    }

    public void setReceiptDetailPrice(float ReceiptDetailPrice) {
        this.ReceiptDetailPrice = ReceiptDetailPrice;
    }

    public float getReceiptDetailDiscount() {
        return ReceiptDetailDiscount;
    }

    public void setReceiptDetailDiscount(float ReceiptDetailDiscount) {
        this.ReceiptDetailDiscount = ReceiptDetailDiscount;
        
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public float getTotalPrice() {
        return TotalPrice;
    }

    public void setTotalPrice(float TotalPrice) {
        this.TotalPrice = TotalPrice;
    }

    public Receipt getReceipt() {
        return receipt;
    }

    public void setReceipt(Receipt receipt) {
        this.receipt = receipt;
    }

    @Override
    public String toString() {
        return "ReceiptDetail{" + "ReceiptDetailId=" + ReceiptDetailId + ", ReceiptId=" + ReceiptId + ", ProductId=" + ProductId + ", product=" + product + ", receipt=" + receipt + ", PromotionId=" + PromotionId + ", ReceiptDetailQuantity=" + ReceiptDetailQuantity + ", ReceiptDetailPrice=" + ReceiptDetailPrice + ", ReceiptDetailDiscount=" + ReceiptDetailDiscount + ", TotalPrice=" + TotalPrice + '}';
    }

    
    
    public static ReceiptDetail fromRS(ResultSet rs) {
            ReceiptDetail receiptDetail = new ReceiptDetail();
            try {
                receiptDetail.setReceiptDetailId(rs.getInt("RD_ID"));
                receiptDetail.setReceiptId(rs.getInt("P_ID"));
                receiptDetail.setProductId(rs.getInt("PD_ID"));
                receiptDetail.setPromotionId(rs.getInt("PT_ID"));
                receiptDetail.setReceiptDetailQuantity(rs.getInt("RD_QUANTITY"));
                receiptDetail.setReceiptDetailPrice(rs.getFloat("RD_PRICE"));
                receiptDetail.setReceiptDetailDiscount(rs.getFloat("RD_DISCOUNT"));
                
            } catch (SQLException ex) {
                Logger.getLogger(Receipt.class.getName()).log(Level.SEVERE, null, ex);
                return null;
            }
            return receiptDetail;
        }

}

