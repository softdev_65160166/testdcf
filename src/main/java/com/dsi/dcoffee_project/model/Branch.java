/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */

package com.dsi.dcoffee_project.model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;


/**
 *
 * @author Surap
 */
public class Branch {
    private int id;
    private String BranchName;
    private String BranchAddress;
    private String BranchPhone;
    private String BranchZipcode;

    public Branch(int id, String BranchName, String BranchAddress, String BranchPhone, String BranchZipcode) {
        this.id = id;
        this.BranchName = BranchName;
        this.BranchAddress = BranchAddress;
        this.BranchPhone = BranchPhone;
        this.BranchZipcode = BranchZipcode;
    }
    public Branch( String BranchName, String BranchAddress, String BranchPhone, String BranchZipcode) {
        this.id = -1;
        this.BranchName = BranchName;
        this.BranchAddress = BranchAddress;
        this.BranchPhone = BranchPhone;
        this.BranchZipcode = BranchZipcode;
    }
     public Branch() {
        this.id = -1;
        this.BranchName = "";
        this.BranchAddress = "";
        this.BranchPhone = "";
        this.BranchZipcode = "";
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getBranchName() {
        return BranchName;
    }

    public void setBranchName(String BranchName) {
        this.BranchName = BranchName;
    }

    public String getBranchAddress() {
        return BranchAddress;
    }

    public void setBranchAddress(String BranchAddress) {
        this.BranchAddress = BranchAddress;
    }

    public String getBranchPhone() {
        return BranchPhone;
    }

    public void setBranchPhone(String BranchPhone) {
        this.BranchPhone = BranchPhone;
    }

    public String getBranchZipcode() {
        return BranchZipcode;
    }

    public void setBranchZipcode(String BranchZipcode) {
        this.BranchZipcode = BranchZipcode;
    }

    @Override
    public String toString() {
        return "Branch{" + "id=" + id + ", BranchName=" + BranchName + ", BranchAddress=" + BranchAddress + ", BranchPhone=" + BranchPhone + ", BranchZipcode=" + BranchZipcode + '}';
    }
    public static Branch fromRS(ResultSet rs) {
        Branch branch = new Branch();
        try {
            branch.setId(rs.getInt("BR_ID"));
            branch.setBranchName(rs.getString("BR_NAME"));
            branch.setBranchAddress(rs.getString("BR_ADDRESS"));
            branch.setBranchPhone(rs.getString("BR_PHONE"));
            branch.setBranchZipcode(rs.getString("BR_ZIPCODE"));
            
        } catch (SQLException ex) {
            Logger.getLogger(Branch.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return branch;
    }
}
