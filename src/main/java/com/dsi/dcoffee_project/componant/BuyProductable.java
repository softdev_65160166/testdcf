/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package com.dsi.dcoffee_project.componant;

import com.dsi.dcoffee_project.model.Product;

/**
 *
 * @author BoM
 */
public interface BuyProductable {
    public void buy(Product product,int qty);
}
